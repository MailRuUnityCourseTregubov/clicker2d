﻿using System;
using UnityEngine;
using Random = UnityEngine.Random;

public class BalloonController : MonoBehaviour {
    [SerializeField] private float growingSpeed;
    [SerializeField] private float startImpulseFrom;
    [SerializeField] private float startImpulseTo;

    // Can not use '+= Time.deltaTime' if 'health' and 'cost' are 'int'
    [SerializeField] private float health;
    [SerializeField] private float healthGrowingSpeed;
    [SerializeField] private float cost;
    [SerializeField] private float costGrowingSpeed;

    private Rigidbody2D rb2d;
    private Renderer rend;

    private GameController gameController;

    private void Start() {
        rb2d = GetComponent<Rigidbody2D>();
        rend = GetComponent<Renderer>();

        // First impulse to start moving (with random sign)
        rb2d.AddForce(new Vector3(
            (Random.Range(0, 2) * 2 - 1) * Random.Range(startImpulseFrom, startImpulseTo),
            (Random.Range(0, 2) * 2 - 1) * Random.Range(startImpulseFrom, startImpulseTo),
            0));
        // Only pure bright colors
        rend.material.color = Random.ColorHSV(0f, 1f,
            1f, 1f, 1f, 1f);

        var gameControllerObject = GameObject.FindWithTag("GameController");
        gameController = gameControllerObject.GetComponent<GameController>();
    }

    private void Update() {
        transform.localScale += new Vector3(growingSpeed, growingSpeed, 0) * Time.deltaTime;
        health += Time.deltaTime * healthGrowingSpeed;
        cost += Time.deltaTime * costGrowingSpeed;
    }

    private void OnMouseDown() {
        --health;
        if (health <= 0) {
            gameController.AddScore(cost);
            var position = gameObject.transform.position;
            Destroy(gameObject);

            // Two or more new instance
            int newBalloonsCount = Math.Max(2, (int) gameObject.transform.localScale.x);
            for (int i = 0; i < newBalloonsCount; i++) {
                Instantiate(gameController.BalloonPrefab, position, Quaternion.identity);
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D other) {
        gameController.GameOver();
    }
}