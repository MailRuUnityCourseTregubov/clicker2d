﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameController : MonoBehaviour {
    [SerializeField] private GameObject balloonPrefab;

    [SerializeField] private Text scoreText;
    [SerializeField] private Text gameOverText;
    [SerializeField] private Text restartText;

    private bool gameOver;
    private float score;

    public GameObject BalloonPrefab => balloonPrefab;

    private void Start() {
        gameOverText.text = "";
        restartText.text = "";
        gameOver = false;
        score = 0;
        UpdateScore();

        var spawnPosition = new Vector3(0, 0, 0);
        Instantiate(balloonPrefab, spawnPosition, Quaternion.identity);
    }

    private void Update() {
        if (gameOver) {
            if (Input.GetKeyDown(KeyCode.R)) {
                Time.timeScale = 1;
                SceneManager.LoadScene(SceneManager.GetActiveScene().name);
            }
        }
    }

    public void AddScore(float value) {
        score += value;
        UpdateScore();
    }

    private void UpdateScore() {
        scoreText.text = "Score: " + (int) score;
    }

    public void GameOver() {
        Time.timeScale = 0;
        var balloons = GameObject.FindGameObjectsWithTag("Balloon");
        foreach (var balloon in balloons) {
            balloon.GetComponent<BalloonController>().enabled = false;
        }

        gameOverText.text = "Game over!";
        restartText.text = "Press 'r' to restart";
        gameOver = true;
    }
}